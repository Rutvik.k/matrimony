package com.rutvik_matrimony.util;

public class Constant {

    public static final int MALE = 1;
    public static final int FEMALE = 2;
    public static final int OTHER = 3;

    public static final String GENDER = "Gender";

    public static final String USER_OBJECT = "UserObject";
    public static final String USER_ID = "userID";

    public static final String FAVORITE_CHANGE_FILTER = "com.rutvik_matrimony.favoritechange";
}
